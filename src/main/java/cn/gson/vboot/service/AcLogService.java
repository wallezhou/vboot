package cn.gson.vboot.service;

import cn.gson.vboot.common.ServiceException;
import cn.gson.vboot.model.mapper.AcLogMapper;
import cn.gson.vboot.model.pojo.AcLog;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * <p>****************************************************************************</p>
 * <p><b>Copyright © 2010-2018 soho team All Rights Reserved<b></p>
 * <ul style="margin:15px;">
 * <li>Description : cn.gson.vboot.service</li>
 * <li>Version     : 1.0</li>
 * <li>Creation    : 2018年09月08日</li>
 * <li>@author     : ____′↘夏悸</li>
 * </ul>
 * <p>****************************************************************************</p>
 */
@Service
@Transactional(readOnly = true, rollbackFor = Exception.class)
public class AcLogService {

    @Autowired
    AcLogMapper acLogMapper;

    public Page<AcLog> getLogList(Integer page) {
        page = Math.max(1, page);
        //分页
        PageHelper.startPage(page, 20, "id desc");
        return acLogMapper.getLogs();
    }

    @Transactional(rollbackFor = Exception.class)
    public void save(AcLog acLog) {
        int effRow;
        effRow = acLogMapper.insert(acLog);

        if (effRow == 0) {
            throw new ServiceException("数据保存失败！");
        }
    }
}
